# test

Test for exploring the GitLab API.  Try to figure out how the GitLab
API works.

这一行用来测试 GitLab 的 Markdown 对中文断行显示是否支持，以对比其与其
他的类似网站，比如 GitHub 和 BitBucket 的区别。因为 BitBucket 不支持中
文断行，而 GitHub 支持得很好。也就是说，当使用中文时，如果将一行断开成
两行，那么 GitHub 上显示时这两行会是连续的，第一行的结尾和第二行的开始
不会有多余空格，而在 BitBucket 上显示时，两行之间会有一个多余的空格。
目前从中文的显示来看，GitLab 能够对中文断行正确显示，这一点上要比
BitBucket 好很多。BitBucket 上中文断行会有多余空格的问题在几年前就存在
了，到现在还未改进，很无语，估计是在中国国内用的人少，且经常无法登录的
缘故吧。

添加 Pull Request 测试。

Pull request commit.
